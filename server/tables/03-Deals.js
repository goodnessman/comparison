let Deals = ['CREATE TABLE IF NOT EXISTS `Deals` (',
  '`Name` varchar(45) DEFAULT NULL,',
  '`id` int(11) NOT NULL AUTO_INCREMENT,',
  '`Text` text,',
  '`ImgURL` varchar(512) DEFAULT NULL,',
  'PRIMARY KEY (`id`)',
  ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
].join('\n');

module.exports = Deals;