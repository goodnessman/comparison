let Channels = [
  'CREATE TABLE IF NOT EXISTS `Channels` (',
  '`Name` TEXT NOT NULL,',
  '`id` int(11) NOT NULL AUTO_INCREMENT,',
  '`Logo` TEXT NOT NULL,',
  '`Category` VARCHAR(45) NOT NULL,',
  '`ChannelMedium` VARCHAR(45) NOT NULL,',
  'PRIMARY KEY (`id`)',
  ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
].join('\n');

module.exports = Channels;