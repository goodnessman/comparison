let Services = [
    'CREATE TABLE IF NOT EXISTS `Services` (',
    '`Name` VARCHAR(45) NULL,',
    '`id` int(11) NOT NULL AUTO_INCREMENT,',
    'PRIMARY KEY (`id`)',
    ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
  ].join('\n');

  module.exports = Services;