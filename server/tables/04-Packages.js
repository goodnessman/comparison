let Packages = [
  'CREATE TABLE IF NOT EXISTS `Packages` (',
  '`id` int(11) NOT NULL AUTO_INCREMENT,',
  '`PackageName` varchar(255) NOT NULL,',
  'PRIMARY KEY (`id`)',
  ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
].join('\n');

module.exports = Packages;