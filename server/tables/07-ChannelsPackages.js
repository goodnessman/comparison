  let ChannelsPackages = [
    'CREATE TABLE IF NOT EXISTS `ChannelsPackages` (',
    '`PackageID` int(11) NOT NULL,',
    '`ChannelID` int(11) NOT NULL,',
    '`id` int(11) NOT NULL AUTO_INCREMENT,',
    'PRIMARY KEY (`id`),',
    'KEY `fk_ChannelsPackages_1_idx` (`ChannelID`),',
    'KEY `fk_ChannelsPackages_2_idx` (`PackageID`),',
    'CONSTRAINT `fk_Channels_1` FOREIGN KEY (`ChannelID`) REFERENCES `Channels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,',
    'CONSTRAINT `fk_Packages_2` FOREIGN KEY (`PackageID`) REFERENCES `Packages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
    ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
  ].join('\n');

  module.exports = ChannelsPackages;