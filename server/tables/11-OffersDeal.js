let OffersDeal = [
  'CREATE TABLE `OffersDeals` (',
  '`idOffer` int(11) DEFAULT NULL,',
  '`idDeal` int(11) DEFAULT NULL,',
  '`id` int(11) NOT NULL AUTO_INCREMENT,',
  'PRIMARY KEY (`id`),',
  'KEY `fk_OfferDeals_1_idx` (`idDeal`),',
  'CONSTRAINT `fk_OfferDeals_1` FOREIGN KEY (`idDeal`) REFERENCES `Offers` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,',
  'CONSTRAINT `fk_OfferDeals_2` FOREIGN KEY (`idDeal`) REFERENCES `Deals` (`id`) ON DELETE SET NULL ON UPDATE CASCADE',
  ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
].join('\n');

module.exports = OffersDeal;