let ISP = ['CREATE TABLE IF NOT EXISTS `ISP` (',
  '`id` int(11) NOT NULL AUTO_INCREMENT,',
  '`ISPName` text NOT NULL,',
  '`Logo` varchar(512) NOT NULL,',
  '`PhoneNumber` varchar(55) NOT NULL,',
  'PRIMARY KEY (`id`)',
  ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
].join('\n');

module.exports = ISP;