let MobilePlan = [
  'CREATE TABLE IF NOT EXISTS `MobilePlan` (',
  '`id` int(11) NOT NULL AUTO_INCREMENT,',
  '`Name` VARCHAR(45) NULL,',
  '`MobileCalls_min` VARCHAR(45) NULL,',
  '`MobileCalls_beyond` VARCHAR(45) NULL,',
  '`MobileData_GB` DECIMAL(5,1) NULL,',
  '`MobileDataBeyond` VARCHAR(45) NULL,',
  '`Speed` VARCHAR(45) NULL,',
  '`IncludedSMS` VARCHAR(45) NULL,',
  '`SMSBeyond` VARCHAR(45) NULL,',
  'PRIMARY KEY (`id`)',
  ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
].join('\n');

module.exports = MobilePlan;