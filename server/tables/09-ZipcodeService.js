let ZipCode = ['CREATE TABLE IF NOT EXISTS `ZipcodeService` (',
  '`zipcode` int(11) DEFAULT NULL,',
  '`ServiceID` int(11) DEFAULT NULL,',
  '`id` int(11) NOT NULL AUTO_INCREMENT,',
  'PRIMARY KEY (`id`),',
  'KEY `fk_ZipcodeService_1_idx` (`ServiceID`),',
  'CONSTRAINT `fk_ZipcodeService_1` FOREIGN KEY (`ServiceID`) REFERENCES `Services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION',
  ') ENGINE=InnoDB DEFAULT CHARSET=utf8;'
].join('\n');

module.exports = ZipCode;
