'use strict';
const co = require('co');
const mysql = require('mysql2');
const options = require('config').mysql;


function* connection() {
  let database = process.env.MYSQL_DATABASE || 'zxcc';
  co(function*() {
    yield this.c.query('CREATE DATABASE IF NOT EXISTS ??', [database]);

  });
}

module.exports = function*(next) {
  // console.log('qwe');
  // yield next;
  let ctx = this;

  let fn = co.wrap(function*(query) {
    let database = process.env.MYSQL_DATABASE || 'exist8';

    let db = yield mysql.createConnectionPromise(options);
    yield db.query('CREATE DATABASE IF NOT EXISTS ??', [database]);
    yield db.query('USE ??', [database]);
    fn.end = db.end.bind(db);
    return yield db.query(query);
  });

  //@@ NEED CLOSE CONNECTION WITH DATA BASE;
  this.db = function(query) {
    return fn(query).then(function(queryResult) {
      fn.end();
      return queryResult;
    });

  }

  // this.db = function(query) {
  //   co(function*() {
  //     let result = yield fn2(query);
  //   }).then(result => result);
  // }


  yield * next;
};
