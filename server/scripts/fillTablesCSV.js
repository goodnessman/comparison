const path = require('path');
const fs = require('fs');
const co = require('co');
const rootPath = require('config').template.serverRoot;
const log = console.log;

let loadData = [
  `LOAD DATA LOCAL INFILE ?`,
  `INTO TABLE $$theTable$$`,
  `FIELDS TERMINATED BY ','`,
  `ENCLOSED BY '"'`,
  `LINES TERMINATED BY '\\n'`,
  `IGNORE 1 LINES`
].join('\n');

module.exports = function*(connection) {
 
  let docsPath = fs.readdirSync(path.join(rootPath, 'docs'));
  // console.log(docsPath)
  let tables = docsPath.map(table => {
    let filePath = path.join(rootPath, 'docs', table);
    return filePath; 
  });

  // console.log(connection);

  console.log(tables);
  co(function*() {
    // var obj = new Options(z'/home/scolustenko/work/comparison/server/docs/01-ISP.csv');
    // fs.writeFileSync('loadData.txt', loadData, 'utf8');
    
    yield tables.map(filePath => {

      
      let table = filePath.split('/').splice(-1)[0].split('.')[0].slice(3);
      let thePath = '.'+filePath.slice(33);

      let query = loadData.replace('$$theTable$$',table);

      console.log(thePath, table);
      return connection.query(query, [thePath, table]);
    });
  }).catch(err=>console.log(err));
};
