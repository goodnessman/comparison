'use strict';
const co = require('co');
const fs = require('fs');
const path = require('path');
const config = require('config');
const modelsRoot = config.template.serverRoot;

//   @@@ TABLES
//   # ISP, Channels, Deals, Packages, MobilePlan, PhonePlan;
//   # ChannelsPackages, Services, ZipCode, Offer, OfferDeal;

module.exports = function*(connection) {
  let tablesDir = fs.readdirSync(path.join(modelsRoot, 'tables'));
  let tables = tablesDir
    .map(tablePath => require(path.join(modelsRoot, 'tables', tablePath)));
  // console.log(tablesDir);
  co(function*() {
    console.log('initialize tables');
    yield* tables.map((table, id) => {
      console.log(`Creating table:  ${tablesDir[id]}`)
      return connection.query(table)     
    });
    yield console.log('Tables ready!')
  });


  // @@ Tables text
  // let fs = require('fs');
  // fs.writeFile('TABLES.txt', tables, 'utf8');
};
