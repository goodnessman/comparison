'use strict';
const Router = require('koa-router');
const qs = require('querystring');
const mysql = require('mysql2');
const co = require('co');
const fs = require('fs');

module.exports = function() {
  return new Router({ prefix: '/api' })

    .get('/', function*() {
      let data = yield this.db('select * from Offers');
        fs.writeFile('TABLES.JSON', data, 'utf8');   

      this.body =  JSON.stringify(data,null,2);
    })
    .get('/products', function*(next) {
      // this.body = 'qwe';
    })
    .get('broadbandTypes', function*(next) {
      console.log(this);
      console.log(qs.parse(this.request.querystring))
    })
    .routes()

}
