'use strict';

const koa = require('koa');
const app = koa();
const fs = require('fs');
const path = require('path');
const config = require('config');
const mysql = require('mysql2');
const co = require('co');

app.keys = 'qwe';

let middlewares = fs.readdirSync(path.join(__dirname, 'server/middlewares')).sort();
middlewares.forEach(function(middleware) {
  console.log(middleware); 
  app.use(require('./server/middlewares/' + middleware))
});

co(function*() {
  let connection = yield mysql.createConnectionPromise(config.mysql);
  yield connection.query('CREATE DATABASE IF NOT EXISTS ??', [database]);
  yield connection.query('USE ??', [database]);
  // yield app.database = database;
  // yield console.log(ctx);
  // console.log( yield connection.execute('select 1+:toAdd as qqq', {toAdd: 10}) );
  yield require('./server/libs/db/loadModels')(connection);
  if (process.env.LOAD_BASE == 'ok') {
    yield require('./server/scripts/f}illTablesCSV')(connection);
  }

  yield connection.end();
  yield console.log('end');
})

app.use(require('./server/libs/rest')());
app.use(require('./server/libs/restAll')());


app.listen(config.server.port, config.server.host,
  () => console.log('Server running at %s:%s', config.server.host, config.server.port));
