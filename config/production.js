module.exports = {
  server: {
    port: env.PORT || 3000,
    host: env.HOST || 'localhost',
    siteHost: env.SITE_HOST || '',
    staticHost: env.STATIC_HOST || ''
  },
  mysql: {
    user: 'root',
    password: '123',
    host: 'localhost',
    database: 'test'
  }
}
