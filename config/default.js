'use strict';

const path = require('path');
const defer = require('config/defer').deferConfig;
let env = process.env;

module.exports = {
  server: {
    port: env.PORT || 3000,
    host: env.HOST || 'localhost',
    siteHost: env.SITE_HOST || '',
    staticHost: env.STATIC_HOST || ''
  },
  mysql: {
    user: 'root',
    password: '1234',
    host: 'localhost',
    port: 3306,
    // database: 'loadToBD',
    namedPlaceholders: true,
    decimalNumbers: true
  },
  projectRoot: process.cwd(),
  template: {
    serverRoot: defer(cfg => path.join(cfg.projectRoot, 'server'))
  }
};
