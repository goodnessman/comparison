(function () {
  'use strict';

  angular
    .module('app.compare')
    .controller('CompareController', CompareController);

    /** @ngInject */
    function CompareController(NgTableParams, $window) {
      var vm = this;

      vm.headerPoppup = false;

      vm.tableData = [{
        operator: {
          icon: 'assets/images/operators/vodafone-99x99.png',
          linkText: 'One 50 Mb | S'
        },
        speed: 50,
        tv: 62,
        fixed: {
          includingFixed: true
        },
        minutes: 200,
        data: {
          data: 2000,
          type: '4g'
        },
        permanence: false,
        promotion: 'assets/images/promotion/roaming gratis.png',
        price: '1',
        contact: '1'
      },
      {
        operator: {
          icon: 'assets/images/operators/orange-99x99.png',
          linkText: 'Canguro Ahorro 300Mb'
        },
        speed: 300,
        tv: 2,
        fixed: {
          includingFixed: true,
          includingMobile: 120
        },
        minutes: 200,
        data:{
          data: 2000,
          type: '4g',
        },
        permanence: 12,
        promotion: 'assets/images/promotion/50_dto_orange.png',
        price: '1',
        contact: '1'
      }];

    //   this.tableSubjectSetupColumns = [
    //   {
    //     title: 'Operador',
    //     field: 'operator',
    //     sortable: true
    //   },
    //   {
    //     title: 'Velocidad',
    //     field: 'speed',
    //     sortable: true
    //   },
    //   {
    //     title: 'TV',
    //     field: 'tv',
    //     sortable: true
    //   },
    //   {
    //     title: 'Fijo',
    //     field: 'fixed',
    //   },
    //   {
    //     title: 'Permanencia',
    //     field: 'permanence',
    //     sortable: true
    //   },
    //   {
    //     title: 'Promoción',
    //     field: 'promotion',
    //   },
    //   {
    //     title: 'Precio',
    //     field: 'price',
    //     sortable: true,
    //     specialContent: true
    //   },
    //   {
    //     title: 'Contacta',
    //     field: 'contact'
    //   }
    // ];

    

  if ($window.innerWidth > 600) {
      vm.tableSubjectSetupColumns = [
      {
        title: 'Operador',
        field: 'operator',
        sortable: true
      },
      {
        title: 'Velocidad',
        field: 'speed',
        sortable: true
      },
      {
        title: 'Fijo',
        field: 'fixed'
      },
      {
        title: 'Minutos',
        field: 'minutes',
        combiner: 'Móvil',
        sortable: true
      },
      {
        title: 'Datos',
        field: 'data',
        sortable: true
      },
      {
        title: 'Permanencia',
        field: 'permanence',
        sortable: true
      },
      {
        title: 'Promoción',
        field: 'promotion'
      },
      {
        title: 'Precio',
        field: 'price',
        sortable: true,
        specialContent: true
      },
      {
        title: 'Contacta',
        field: 'contact'
      }
    ];
  }else {
      vm.tableSubjectSetupColumns = [
      {
        title: 'Operador',
        field: 'operator',
        sortable: true
      },
      {
        title: 'Velocidad',
        field: 'speed',
        sortable: true
      },
      {
        title: 'Permanencia',
        field: 'permanence',
        sortable: true
      },
      {
        title: 'Precio',
        field: 'price',
        sortable: true,
        specialContent: true
      },
      {
        title: 'Contacta',
        field: 'contact'
      }
    ];
  }

    

      vm.tableObj = new NgTableParams({
        count: vm.tableData.length
      }, {
        data: vm.tableData,
        counts: []
      });
    }
})();
