(function () {
    'use strict';

        angular.module('app.compare')
        .directive('moreFilters', function () {
            return {
                restrict: 'E',
                templateUrl: 'app/modules/compare/moreFilters.html',
                link: function (scope, element) {
                    var button = angular.element(element[0].querySelector('#more_filters_btn')),
                    section = angular.element(element[0].querySelector('#filters'));

                    button.on('click', function() {
                        section.toggleClass('disclosed')
                    })
                }
            }
        })
        .directive('selectOption', function () {
            return {
                restrict: 'A',
                link: function (scope, element) {
                    var el = angular.element(element),
                    input = element[0].querySelector('input');

                    el.on('click', function() {
                        if (angular.element(input).prop('checked')) {
                            el.addClass('selected');
                        }else {
                            el.removeClass('selected');
                        }
                    });
                }
            };
        })
        .directive('selectOptionRadio', function () {
            return {
                restrict: 'A',
                link: function (scope, element) {
                    // debugger;
                    var el = angular.element(element),
                    labels = el.find('label');

                    angular.forEach(labels, function(value) {

                      angular.element(value).on('click', function() {
                        clearAllOption();
                        angular.element(value).addClass('selected');
                      });

                    });

                    function clearAllOption() {
                        angular.forEach(labels, function(value) {
                            angular.element(value).removeClass('selected');
                        });
                    }
                }
            };
        })
        .directive('expandedHeaderMenu', function () {
            return {
                restrict: 'A',
                link: function (scope, element) {
                    var el = angular.element(element),
                    poppup = element[0].querySelector('#nav_recently_viewed');

                    el.on('click', function() {
                         angular.element(poppup).addClass('menu-expanded');
                    });
                }
            };
        })
        .directive('postcodeHolder', function () {
            return {
                restrict: 'A',
                link: function (scope, element) {
                    var link = angular.element(element[0].querySelector('#edit_postcode'));

                    link.on('click', function(e) {
                        e.preventDefault();
                         angular.element(element[0]).html('<form><input type="text" name="location" placeholder="13234"><button type="submit" class="click-btn">Ver</button></form> ');
                    });
                }
            };
        });
})();
