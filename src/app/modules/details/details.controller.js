(function () {
  'use strict';

  angular
    .module('app.details')
    .controller('DetailsController', DetailsController)
    .controller('TabsDemoCtrl', function ($scope) {
          $scope.tabs = [
            { title:'Dynamic Title 1', content:'Dynamic content 1' },
            { title:'Dynamic Title 2', content:'Dynamic content 2' }
          ];

          $scope.model = {
            name: 'Tabs'
          };
        });

    /** @ngInject */
    function DetailsController($scope) {
      var vm = this;

      vm.nationalTarifes = true;
      vm.internationalTarifes = false;
      vm.internaMobileTarifes = false;
      vm.checkedTarifePrice = 'fijos nacionales';
      vm.showNameChanged = showNameChanged;

      function showNameChanged () {
      	if (vm.checkedTarifePrice == 0) {
      		vm.nationalTarifes = true;
      		vm.internationalTarifes = false;
      		vm.internaMobileTarifes = false;
      	}else if (vm.checkedTarifePrice == 1) {
      		vm.nationalTarifes = false;
      		vm.internationalTarifes = true;
      		vm.internaMobileTarifes = false;
      	}else {
      		vm.nationalTarifes = false;
      		vm.internationalTarifes = false;
      		vm.internaMobileTarifes = true;
      	}
      }

      vm.onClickTab = function (tab) {
          vm.currentTab = tab.url;
      };

      vm.isActiveTab = function(tabUrl) {
          return tabUrl == vm.currentTab;
      };
    }
})();
