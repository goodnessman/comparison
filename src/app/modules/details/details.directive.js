(function () {
    'use strict';

        angular.module('app.details')
        .directive('gratisCheck', function () {
            return {
                restrict: 'E',
                templateUrl: 'app/modules/details/gratisCheck.html',
                link: function (scope, element) {

                    scope.gratisCheck = false;

                    var checkbox = angular.element(element[0].querySelector('#checkGratis'));

                    checkbox.on('click', function() {
                            scope.$apply(function() {
                                scope.gratisCheck = !scope.gratisCheck;
                            });
                            checkbox.toggleClass('line-rental-included');
                            checkbox.toggleClass('line-rental-excluded');
                    });
                }
            };
        });
})();