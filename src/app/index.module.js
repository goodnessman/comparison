(function() {
  'use strict';

  angular
    .module('app', [

    /* vendor modules*/

    'ngAnimate',
    'ngCookies',
    'ngTouch',
    'ngSanitize',
    'ngMessages',
    'ngAria',
    'ngTable',
    'ui.router',
    'ui.bootstrap',
    'toastr',

    /* app modules */

    'app.details',
    'app.compare'
  ]);

})();
