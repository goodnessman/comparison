(function() {
  'use strict';

  angular
    .module('app')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    // 
    //  $locationProvider.html5Mode({
    //     enabled: true,
    //     requireBase: false
    // });

    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })
      .state('main.compare', {
        url: 'compare',
        templateUrl: 'app/modules/compare/compare.html',
        controller: 'CompareController',
        controllerAs: 'vm'
      })
      .state('main.details', {
        url: 'details/:id',
        templateUrl: 'app/modules/details/details.html',
        controller: 'DetailsController',
        controllerAs: 'vm'
      });

    $urlRouterProvider.otherwise('/compare');
  }

})();
