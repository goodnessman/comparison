(function() {
  'use strict';

  angular
    .module('app')
    .directive('elwidthresize', ['$window', function($window) {
        return {
            restrict: 'A',
            scope: {
                type: '@'
            },
            link: function(scope, elem, attrs) {
                scope.onResize = function() {
                    var block = angular.element(elem);

                    attrs.$observe('type', function(value) {
                        if ($window.innerWidth > 600 && value === 'mobile') {
                            block.addClass('hide');
                        }else if ($window.innerWidth < 600 && value === 'desctop') {
                            block.addClass('hide');
                        }else if ($window.innerWidth > 600 && value === 'desctop') {
                            block.removeClass('hide');
                        }else if ($window.innerWidth < 600 && value === 'mobile') {
                            block.removeClass('hide');
                        }
                    });

                    
                };
                scope.onResize();

                angular.element($window).bind('resize', function() {
                    scope.onResize();
                });
            }
        };
    }]);
})();
